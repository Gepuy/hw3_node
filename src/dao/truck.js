/* eslint-disable */
const TruckSource = require('./source/mongodb/Truck');

class Truck {
  constructor(source) {
    this.source = source;
  }

  async getAll(id) {
    return await this.source.getAll(id);
  }

  async create(data) {
    await this.source.create(data);
  }

  async getOne(id) {
    return await this.source.getOne(id);
  }

  async updateById(id, updateConfig) {
    return await this.source.updateById(id, updateConfig);
  }

  async updateByField(field, data) {
    return await this.source.updateByField(field, data);
  }

  async delete(id) {
    return await this.source.delete(id);
  }

  async assign(userId, truckId) {
    return await this.source.assign(userId, truckId);
  }

  async unassign(id) {
    return await this.source.unassign(id);
  }

  async getAssignedTruck(id) {
    return await this.source.getAssignedTruck(id);
  }

  async getAssignedTrucks() {
    return await this.source.getAssignedTrucks();
  }
}

module.exports = new Truck(TruckSource);

/* eslint-disable no-return-await */
const MailSource = require('./source/smtp/Mail');

class Mail {
  constructor(source) {
    this.source = source;
  }

  async sendNewPassword(email, password) {
    return await this.source.sendNewPassword(email, password);
  }
}

module.exports = new Mail(MailSource);

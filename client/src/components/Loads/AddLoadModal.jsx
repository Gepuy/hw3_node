import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {useAction} from "../../store/hooks/useAction";

const AddLoad = ({show, onHide}) => {
  const [name, setName] = useState('');
  const [payload, setPayload] = useState(0);
  const [pickupAddress, setPickupAddress] = useState('');
  const [deliveryAddress, setDeliveryAddress] = useState('');
  const [loadWidth, setLoadWidth] = useState('');
  const [loadHeight, setLoadHeight] = useState('');
  const [loadLength, setLoadLength] = useState('');
  const [error, setError] = useState('');

  const {addLoad} = useAction();

  const createLoad = () => {
    const load = {
      name,
      payload,
      pickup_address: pickupAddress,
      delivery_address: deliveryAddress,
      dimensions: {
        width: loadWidth,
        height: loadHeight,
        length: loadLength
      }
    };
    addLoad(load).then(() => {
      onHide();
    }).catch((e) => setError(e.response.data.message));
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size={'lg'}
      centered
    >
      <Modal.Header closeButton style={{textAlign: "center"}}>
        <Modal.Title>Create new load</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form className={'d-flex flex-column'}>
          <Form.Control
            value={name}
            onFocus={() => setError('')}
            placeholder={'Enter load name'}
            onChange={(e) => setName(e.target.value)}
          />
          <Form.Control
            className={'mt-3'}
            onFocus={() => setError('')}
            value={payload}
            type={'number'}
            placeholder={'Enter load payload'}
            onChange={(e) => setPayload(e.target.value)}
          />
          <Form.Control
            className={'mt-3'}
            onFocus={() => setError('')}
            value={pickupAddress}
            placeholder={'Enter load pickup address'}
            onChange={(e) => setPickupAddress(e.target.value)}
          />
          <Form.Control
            className={'mt-3'}
            onFocus={() => setError('')}
            value={deliveryAddress}
            placeholder={'Enter load delivery address'}
            onChange={(e) => setDeliveryAddress(e.target.value)}
          />
          <div className={'d-flex mt-3'}>
            <Form.Control
              value={loadWidth}
              onFocus={() => setError('')}
              type={'number'}
              placeholder={'Enter load width'}
              onChange={(e) => setLoadWidth(e.target.value)}
            />
            <Form.Control
              type={'number'}
              onFocus={() => setError('')}
              value={loadHeight}
              placeholder={'Enter load height'}
              onChange={(e) => setLoadHeight(e.target.value)}
            />
            <Form.Control
              type={'number'}
              onFocus={() => setError('')}
              value={loadLength}
              placeholder={'Enter load length'}
              onChange={(e) => setLoadLength(e.target.value)}
            />
          </div>
        </Form>
        {error ? <p style={{margin: "0", color: "red"}}>{error}</p> : null}
      </Modal.Body>
      <Modal.Footer>
        <Button variant={'outline-secondary'} onClick={onHide}>Close</Button>
        <Button variant={'outline-success'} onClick={() => createLoad()}>Add Load</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddLoad;

import React from 'react';
import {NavLink} from "react-router-dom";
import {ASSIGNED_LOADS_ROUTE, LOADS_ROUTE} from "../../utils/consts";
import styles from '../DriverPage/driverpage.module.css';
import {FaTruckLoading} from "react-icons/fa";
import {TbTruckLoading} from "react-icons/tb";

const ShipperSideBar = () => {
  return (
    <div style={{width: "22%", borderRight: '2px solid #CFD2CF'}}>
      <div className={'d-flex flex-column'}
           style={{fontSize: "19px"}}
      >
        <NavLink
          to={LOADS_ROUTE}
          className={(navData) => navData.isActive ? styles.active : styles.navlink}
        >
          <TbTruckLoading size={25}/> All loads
        </NavLink>

        <NavLink
          to={ASSIGNED_LOADS_ROUTE}
          className={(navData) => navData.isActive ? styles.active : styles.navlink}
        >
          <FaTruckLoading/> Assigned Loads
        </NavLink>
      </div>
    </div>
  );
};

export default ShipperSideBar;

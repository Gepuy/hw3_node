import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {forgetPassword} from "../../../api/userApi";

const ForgetPassword = ({show, onHide}) => {
  const [error, setError] = useState('');
  const [email, setEmail] = useState('');

  const sendNewPassword = async (email) => {
    try {
      await forgetPassword(email);
      setEmail('');
      onHide();
    } catch (e) {
      setError(e.response.data.message)
    }
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size={'sm'}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Forget password?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder={'Enter your email'}
            onFocus={() => setError('')}
          />
        </Form>
        {error ? <p style={{margin: "0", color: "red"}}>{error}</p> : null}
      </Modal.Body>
      <Modal.Footer>
        <Button variant={'outline-secondary'} onClick={onHide}>Close</Button>
        <Button
          variant={'outline-success'}
          onClick={() => sendNewPassword(email)}
        >Confirm</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ForgetPassword;

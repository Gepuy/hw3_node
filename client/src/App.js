import NavBar from "./components/NavBar";
import AppRouter from "./components/AppRouter";

function App() {
  return (
    <div>
      <NavBar/>
      <AppRouter/>
    </div>
  );
}

export default App;

const jwt = require('jsonwebtoken');

const generateAccessToken = (id, role, email, createdDate) => {
  const payload = {
    id, role, email, createdDate,
  };
  return jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn: '24h' });
};

module.exports = generateAccessToken;

import {ActionTypes} from "../consts/ActionTypes";

const initialValue = {
  trucks: {},
};

export const truckReducer = (state = initialValue, action) => {
  switch (action.type) {
    case (ActionTypes.SET_DRIVER_TRUCKS):
      return {...state, trucks: action.payload};
    case (ActionTypes.DELETE_USER_TRUCK):
      return {...state, trucks: state.trucks.filter(truck => truck._id !== action.payload)};
    default:
      return state;
  }
}


import React from 'react';
import {Button, Card} from "react-bootstrap";
import {AiFillDelete} from "react-icons/ai";
import {useAction} from "../../store/hooks/useAction";
import {useSelector} from "react-redux";

const Load = ({id, name, payload, pickup_address, delivery_address, state, status}) => {
  const {postLoad, deleteLoad, iterateLoadState} = useAction();
  const user = useSelector(state => state.user.user);

  return (
    <Card style={{width: '45%', boxShadow: "0px 0px 6px rgba(69, 55, 61, 0.7)", padding: '20px 40px'}}>
      <h5 style={{textAlign: "center"}}>{name}</h5>
      {state ? <small style={{textAlign: "center", fontWeight: "bold"}}>{state}</small> : null}
      <div>
        <p style={{fontSize: '14px', margin: '10px 0 0'}}><strong>Pickup Address:</strong> {pickup_address}</p>
        <p style={{fontSize: '14px', margin: '5px 0'}}><strong>Delivery Address:</strong> {delivery_address}</p>
      </div>
      <p style={{fontSize: '14px', margin: '5px 0'}}><strong>Payload:</strong> {payload}</p>
      {status === 'NEW' &&
        <div className={'d-flex justify-content-between align-items-center mt-4'} style={{columnGap: "17px"}}>
          <Button
            variant={'outline-secondary'}
            className={'w-50'}
            onClick={() => postLoad(id)}
          >Post load</Button>
          <AiFillDelete
            style={{cursor: "pointer"}}
            size={25}
            onClick={() => deleteLoad(id)}
          />
        </div>
      }
      {
        user.role === 'DRIVER' && status !== 'SHIPPED' &&
        <div className={'d-flex justify-content-end'}>
          <Button onClick={iterateLoadState}>Iterate state</Button>
        </div>
      }
    </Card>
  );
};

export default Load;

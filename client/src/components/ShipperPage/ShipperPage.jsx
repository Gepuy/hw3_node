import React from 'react';
import ShipperSideBar from "./ShipperSideBar";
import ShipperRouter from "./ShipperRouter";

const ShipperPage = () => {
  return (
    <>
      <div className={'d-flex'} style={{minHeight: window.innerHeight - 54.5}}>
        <ShipperSideBar/>
        <div style={{width: "78%"}}>
          <ShipperRouter/>
        </div>
      </div>
    </>
  );
};

export default ShipperPage;

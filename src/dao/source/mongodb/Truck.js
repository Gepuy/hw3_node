/* eslint-disable class-methods-use-this */
const mongoose = require('mongoose');
const { Truck: TruckModel } = require('../../../models/truck-model');
const { TruckNotFound, CannotChangeTruck, CannotDeleteTruck } = require('../../../error/daoError');

class Truck {
  async create(data) {
    await TruckModel.create(data);
  }

  async getAll(id) {
    const trucks = await TruckModel.find({ created_by: id });
    return trucks;
  }

  async getOne(id) {
    const truck = await TruckModel.findById(id);
    if (!truck) {
      throw TruckNotFound;
    }
    return truck;
  }

  async updateById(id, data) {
    const truck = await TruckModel.findByIdAndUpdate(id, data);
    if (!truck) {
      throw TruckNotFound;
    }

    if (truck.status === 'OL') {
      throw CannotChangeTruck;
    }
    return truck;
  }

  async updateByField(field, data) {
    const truck = await TruckModel.updateOne(field, data);
    if (!truck) {
      throw TruckNotFound;
    }
    return truck;
  }

  async delete(id) {
    const truck = await TruckModel.findByIdAndDelete(id);
    if (!truck) {
      throw TruckNotFound;
    }

    if (truck.status === 'OL') {
      throw CannotDeleteTruck;
    }
  }

  async assign(userId, truckId) {
    const assignedTruck = await TruckModel.findByIdAndUpdate(truckId, {
      assigned_to: new mongoose.mongo.ObjectId(userId),
    });
    if (!assignedTruck) {
      throw TruckNotFound;
    }
    return assignedTruck;
  }

  async unassign(id) {
    const unassignedTruck = await TruckModel.findByIdAndUpdate(id, { assigned_to: null });
    return unassignedTruck;
  }

  async getAssignedTruck(id) {
    const assignedTruck = await TruckModel.findOne({
      assigned_to: new mongoose.mongo.ObjectId(id),
    });
    return assignedTruck;
  }

  async getAssignedTrucks() {
    const assignedTrucks = await TruckModel.aggregate([
      {
        $match: {
          status: 'IS',
          assigned_to: {
            $ne: null,
          },
        },
      },
    ]);
    return assignedTrucks;
  }
}

module.exports = new Truck();

import React from 'react';
import {useSelector} from "react-redux";
import Load from "./Load";

const AssignedLoads = () => {
  const loads = useSelector(state => state.userLoads.loads);

  return (
    <div>
      <h3 className={'mt-3'} style={{textAlign: "center"}}>Assigned loads</h3>
      <div className={'d-flex flex-wrap mt-4'} style={{columnGap: "17px", rowGap: "17px", marginLeft: '6vw'}}>
        {loads.filter(el => el.status === 'ASSIGNED').map(load => (
          <Load
            key={load._id}
            id={load._id}
            name={load.name}
            payload={load.payload}
            pickup_address={load.pickup_address}
            delivery_address={load.delivery_address}
            state={load.state}
            status={load.status}
          />
        ))}
      </div>
    </div>
  );
};

export default AssignedLoads;

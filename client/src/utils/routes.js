import {
  LOGIN_ROUTE,
  REGISTRATION_ROUTE,
  USER_ROUTE,
  MAIN_ROUTE,
  TRUCKS_ROUTE,
  LOADS_ROUTE,
  ASSIGNED_LOADS_ROUTE
} from './consts'
import AuthPage from "../pages/AuthPage";
import UserPage from "../pages/UserPage";
import MainPage from "../pages/MainPage";
import Trucks from "../components/Trucks/Trucks";
import Loads from "../components/Loads/Loads";
import AssignedLoads from "../components/Loads/AssignedLoads";

export const authRoutes = [
  {
    path: USER_ROUTE,
    Component: UserPage,
  },
]

export const publicRoutes = [
  {
    path: LOGIN_ROUTE,
    Component: AuthPage,
  },
  {
    path: REGISTRATION_ROUTE,
    Component: AuthPage,
  },
  {
    path: MAIN_ROUTE,
    Component: MainPage
  },
]

export const driverRoutes = [
  {
    path: TRUCKS_ROUTE,
    Component: Trucks
  },
  {
    path: LOADS_ROUTE,
    Component: Loads
  }
]

export const shipperRoutes = [
  {
    path: LOADS_ROUTE,
    Component: Loads
  },
  {
    path: ASSIGNED_LOADS_ROUTE,
    Component: AssignedLoads
  }
]

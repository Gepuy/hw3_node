import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {useAction} from "../../store/hooks/useAction";
import Load from "./Load";
import {Button} from "react-bootstrap";
import AddLoadModal from "./AddLoadModal";

const Loads = () => {
  const user = useSelector(state => state.user.user);
  const loads = useSelector(state => state.userLoads.loads);
  const {setUserLoads} = useAction();
  const [addLoadVisible, setAddLoadVisible] = useState(false);

  useEffect(() => {
    setUserLoads().then();
  }, []);

  return (
    <div>
      {user.role === 'DRIVER' ?
        <h3 className={'mt-3'} style={{textAlign: "center"}}>Completed and active loads</h3>
        :
        <h3 className={'mt-3'} style={{textAlign: "center"}}>All loads</h3>
      }
      {loads.length ?
        <div className={'d-flex flex-column'}>
          <div className={'d-flex flex-wrap mt-4'} style={{columnGap: "17px", rowGap: "17px", marginLeft: '6vw'}}>
            {loads.map(load => (
              <Load
                key={load._id}
                id={load._id}
                name={load.name}
                payload={load.payload}
                pickup_address={load.pickup_address}
                delivery_address={load.delivery_address}
                state={load.state}
                status={load.status}
              />
            ))}
          </div>

        </div>
        :
        <h5 style={{textAlign: "center", width: '100%'}}>No one load has not found</h5>
      }
      {user.role === 'SHIPPER' &&
        <div className={'d-flex justify-content-center'}>
          <Button
            className={'my-3 '}
            onClick={() => setAddLoadVisible(true)}
          >Add new load</Button>
          <AddLoadModal show={addLoadVisible} onHide={() => setAddLoadVisible(false)}/>
        </div>
      }
    </div>
  );
};

export default Loads;

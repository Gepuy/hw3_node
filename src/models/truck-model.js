/* eslint-disable linebreak-style */
/* eslint-disable indent */
const { Schema, model } = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const truckJoiSchema = Joi.object({
    created_by: Joi.objectId(),
    assigned_to: Joi.objectId(),
    type: Joi.string().required(),
    status: Joi.string(),
});

const TruckSchema = new Schema({
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
    },
    assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        default: null,
    },
    type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        default: 'SPRINTER',
    },
    status: {
        type: String,
        enum: ['OL', 'IS'],
        default: 'IS',
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
}, { versionKey: false });

module.exports = {
    Truck: model('Truck', TruckSchema),
    truckJoiSchema,
};

import {useSelector} from "react-redux";
import DriverPage from "../components/DriverPage/DriverPage";
import ShipperPage from "../components/ShipperPage/ShipperPage";

const NotesPage = () => {
  const user = useSelector(state => state.user);

  return (
    <div
      className={'bg-light'}
      style={{minHeight: window.innerHeight - 54.5}}
    >
      {user.isAuth ?
        user.user.role === 'DRIVER'
          ? <DriverPage/>
          : <ShipperPage/>
        :
        <div
          className={'d-flex justify-content-center align-items-center'}
          style={{height: "90vh"}}
        >
          <h2>You need to be authorized</h2>
        </div>
      }
    </div>
  );
};

export default NotesPage;

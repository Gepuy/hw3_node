/* eslint-disable linebreak-style */
const Router = require('express');
// const joiMiddleware = require('../middleware/joiMiddleware');
// const { truckJoiSchema } = require('../models/truck-model');

const router = new Router();
const truckController = require('../controllers/truck-controller');

router.get('/', truckController.getUserTrucks);
router.post('/', truckController.addTruckForUser);
router.get('/:id', truckController.getOneUserTruck);
router.put('/:id', truckController.updateUserTruck);
router.delete('/:id', truckController.deleteUserTruck);
router.post('/:id/assign', truckController.assignTruckToUser);

module.exports = router;

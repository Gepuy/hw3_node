import {ActionTypes} from "../consts/ActionTypes";

const initialValue = {
  loads: {},
};

export const loadReducer = (state = initialValue, action) => {
  switch (action.type) {
    case (ActionTypes.SET_USER_LOADS):
      return {...state, loads: action.payload};
    default:
      return state;
  }
}


/* eslint-disable linebreak-style */
const Router = require('express');
const loadController = require('../controllers/load-controller');
const roleMiddleware = require('../middleware/roleMiddleware');

const router = new Router();
const DRIVER = 'DRIVER';
const SHIPPER = 'SHIPPER';

router.get('/', loadController.getUserLoads);
router.post('/', roleMiddleware(SHIPPER), loadController.addUserLoad);
router.get('/active', roleMiddleware(DRIVER), loadController.getUserActiveLoad);
router.patch('/active/state', roleMiddleware(DRIVER), loadController.iterateNextLoadState);
router.get('/:id', loadController.getUserLoadById);
router.put('/:id', roleMiddleware(SHIPPER), loadController.updateUserLoadById);
router.delete('/:id', roleMiddleware(SHIPPER), loadController.deleteUserLoadById);
router.post('/:id/post', roleMiddleware(SHIPPER), loadController.postUserLoadById);
router.get('/:id/shipping_info', roleMiddleware(SHIPPER), loadController.getUserLoadShippingInfo);

module.exports = router;

/* eslint-disable */
const Load = require('../dao/load');
const Truck = require('../dao/truck');
const { loadJoiSchema } = require('../models/load-model');
const ApiError = require('../error/ApiError');
const iterateToNextState = require('../utils/loadState');
const { getTruckType } = require('../utils/truckTypes');

class LoadController {
  async getUserLoads(req, res, next) {
    const maxLimitValue = 50;
    const user = req.user;
    let { status, limit=10, offset=0 } = req.query;

    if (limit > maxLimitValue) limit = maxLimitValue;

    if (user.role === 'DRIVER') {
      let driverLoads = await Load.getUserAllAssigned(user.id, +limit, +offset, status);
      return res.json({loads: driverLoads});
    } else {
      const loads = await Load.getAll(user.id, +limit, +offset, status);
      return res.json({loads});
    }
  }

  async addUserLoad(req, res, next) {
    try {
      await loadJoiSchema.validateAsync(req.body);
      await Load.create({
        created_by: req.user.id,
        ...req.body
      });
      return res.json({message: 'Load created successfully'});
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async getUserActiveLoad(req, res, next) {
    try {
      let activeLoad = await Load.getActive(req.user.id);
      return res.json({load: activeLoad});
    } catch (e) {
      return next(ApiError.badRequest(e.message))
    }
  }

  async iterateNextLoadState(req, res, next) {
    try {
      const user = req.user;
      const activeLoad = await Load.getActive(user.id);

      const newLoadState = iterateToNextState(activeLoad.state);

      if(newLoadState === 'Arrived to delivery') {
        await Truck.updateByField({assigned_to: user.id}, {status: 'IS'});
        await Load.updateDeliveredLoadState(user.id, activeLoad.id, newLoadState);
      }

      await Load.updateLoadStateLogs(user.id, activeLoad.id, newLoadState);

      res.json({ message: `Load state changed to ${newLoadState}` });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async getUserLoadById(req, res, next) {
    try {
      const load = await Load.getOne(req.params.id);
      return res.json({load});
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async updateUserLoadById(req, res, next) {
    try {
      await loadJoiSchema.validateAsync(req.body);
      await Load.updateById(req.params.id, req.body);
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Load details changed successfully' });
  }

  async deleteUserLoadById(req, res, next) {
    try {
      await Load.deleteById(req.params.id);
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }

    res.status(200).json({ message: 'Load deleted successfully' })
  }

  async postUserLoadById(req, res, next) {
    try {
      const newStatus = 'NEW';
      const postedStatus = 'POSTED'
      const loadId = req.params.id;
      const load = await Load.getOne(loadId);
      const assignedTrucks = await Truck.getAssignedTrucks();

      if(load.status !== newStatus) {
        return next(ApiError.badRequest('This load is already active'));
      }

      await Load.updateLoadStatus(loadId, postedStatus);

      const foundTruck = assignedTrucks[0];
      const truckData = foundTruck ? getTruckType(foundTruck.type) : null;

      if (truckData) {
        Load.compareTruckAndLoadParams(truckData, load)
        await Truck.updateById(foundTruck._id, {status: 'OL'});
        await Load.setAssigned(loadId, foundTruck);

        return res.json({
          message: 'Load posted successfully',
          driver_found: true,
        });
      } else {
        await Load.updateLoadStatus(loadId, newStatus);
        return next(ApiError.badRequest('There is no free drivers'));
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async getUserLoadShippingInfo(req, res, next) {
    try {
      const load = await Load.getOne(req.params.id);
      let truck = null;

      if( load.assigned_to !== null ) {
        truck = await Truck.getAssignedTruck(load.assigned_to);
      }

      if(!truck) {
        return next(ApiError.badRequest('Truck has not found yet'));
      }
      res.status(200).json({
        load,
        truck
      })
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new LoadController();

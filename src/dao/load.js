/* eslint-disable no-return-await */
const LoadSource = require('./source/mongodb/Load');

class Load {
  constructor(source) {
    this.source = source;
  }

  async create(data) {
    await this.source.create(data);
  }

  async getAll(id, limit, offset, status) {
    return await this.source.getAll(id, limit, offset, status);
  }

  async getUserAllAssigned(id, limit, offset, status) {
    return await this.source.getUserAllAssigned(id, limit, offset, status);
  }

  async getOne(id) {
    return await this.source.getOne(id);
  }

  async getActive(id) {
    return await this.source.getActive(id);
  }

  async updateById(id, data) {
    await this.source.updateById(id, data);
  }

  async updateByField(field, data) {
    return await this.source.updateByField(field, data);
  }

  async updateLoadStateLogs(userId, loadId, loadState) {
    return await this.source.updateLoadStateLogs(userId, loadId, loadState);
  }

  async updateLoadStatus(loadID, status) {
    return await this.source.updateLoadStatus(loadID, status);
  }

  async updateDeliveredLoadState(userId, loadId, loadState) {
    return await this.source.updateDeliveredLoadState(userId, loadId, loadState);
  }

  async deleteById(loadId) {
    await this.source.deleteById(loadId);
  }

  async setAssigned(loadId, truck) {
    await this.source.setAssigned(loadId, truck);
  }

  compareTruckAndLoadParams(truck, load) {
    return this.source.compareTruckAndLoadParams(truck, load);
  }
}

module.exports = new Load(LoadSource);

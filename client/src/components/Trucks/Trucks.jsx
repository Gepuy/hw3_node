import React, {useEffect, useState} from 'react';
import {Button} from "react-bootstrap";
import {useSelector} from "react-redux";
import {useAction} from "../../store/hooks/useAction";
import Truck from "./Truck";
import AddTruckModal from "./AddTruckModal";

const Trucks = () => {
  const trucks = useSelector(state=> state.driverTrucks.trucks);
  const {setDriverTrucks} = useAction();
  const [truckModal, setTruckModal] = useState(false);

  useEffect(() => {
    setDriverTrucks().then();
  }, []);

  return (
    <div className={'d-flex flex-column'}>
      <h3 className={'mt-3'} style={{textAlign: "center"}}>My trucks</h3>
      <div className={'d-flex flex-column mt-4'}>
        {trucks.length ? trucks.map((truck) => (
          <Truck
            key={truck._id}
            id={truck._id}
            type={truck.type}
            assigned_to={truck.assigned_to}
            status={truck.status}
          />
        )) : <h5 style={{textAlign: "center"}}>You have not added trucks yet</h5>}
      </div>
      <Button
        className={'mt-2 mb-4 align-self-center'}
        onClick={() => setTruckModal(true)}
      >Add new truck</Button>
      <AddTruckModal
        show={truckModal}
        onHide={() => setTruckModal(false)}
      />
    </div>
  );
};

export default Trucks;

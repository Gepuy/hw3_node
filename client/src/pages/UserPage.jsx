import React, {useState} from 'react';
import {Button, Card} from "react-bootstrap";
import {VscAccount} from "react-icons/vsc";
import {useSelector} from "react-redux";
import ChangePassword from "../components/Account/modals/ChangePassword";
import DeleteAccount from "../components/Account/modals/DeleteAccount";
import UpdatePhoto from "../components/Account/modals/UpdatePhoto";

const UserPage = () => {
  const user = useSelector(state => state.user.user);
  const [changePasswordVisible, setChangePasswordVisible] = useState(false);
  const [deleteAccountVisible, setDeleteAccountVisible] = useState(false);
  const [updateImageVisible, setUpdateImageVisible] = useState(false)

  return (
    <div
      className="d-flex justify-content-center align-items-center bg-light"
      style={{minHeight: window.innerHeight - 54.5}}
    >
      <Card className={'p-4'} style={{width: "500px", height: "500px"}}>
        <div className={'d-flex align-items-center justify-content-evenly flex-column h-100'}>
          {user.img ?
            <img
              src={`${process.env.REACT_APP_BACKEND_API_URL}/${user.img}`}
              alt={'user avatar'}
              style={{maxWidth: "300px", maxHeight: "150px", borderRadius: "10px"}}
            />
            : <VscAccount size={130}/>}
          <div className="d-flex flex-column align-items-center">
            <h4 className={'mt-3'}>{user?.email}</h4>
            <h5 className={'mt-3'}>{user?.role}</h5>
          </div>
          <div className={'d-flex justify-content-evenly flex-wrap w-75 mt-2'}>
            <Button
              style={{width: '160px'}}
              onClick={() => setChangePasswordVisible(true)}
            >Change password</Button>
            <Button
              style={{width: '160px'}}
              onClick={() => setUpdateImageVisible(true)}
            >Change photo</Button>

            <Button
              className={'mt-3'}
              style={{width: '160px'}}
              onClick={() => setDeleteAccountVisible(true)}
              variant={'danger'}
            >Delete account</Button>
          </div>
        </div>
      </Card>
      <ChangePassword
        show={changePasswordVisible}
        onHide={() => setChangePasswordVisible(false)}
      />
      <DeleteAccount
        show={deleteAccountVisible}
        onHide={() => setDeleteAccountVisible(false)}/>
      <UpdatePhoto
        show={updateImageVisible}
        onHide={() => setUpdateImageVisible(false)}
      />
    </div>
  );
};

export default UserPage;

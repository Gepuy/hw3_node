import React from 'react';
import {ListGroup} from "react-bootstrap";

const UserRole = ({setUserRole}) => {
  return (
    <ListGroup horizontal className={'mt-3'}>
      <ListGroup.Item
        style={{cursor: "pointer", width: "50%", textAlign: "center"}}
        href={'#link1'}
        onClick={() => setUserRole('DRIVER')}
      >
        I want to be a driver</ListGroup.Item>
      <ListGroup.Item
        onClick={() => setUserRole('SHIPPER')}
        style={{cursor: "pointer", width: "50%", textAlign: "center"}}
        href={'#link2'}
      >I want to be a shipper</ListGroup.Item>
    </ListGroup>
  );
};

export default UserRole;

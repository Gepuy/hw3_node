import {combineReducers} from "redux";
import {userReducer} from "./userReducer";
import {truckReducer} from './truckReducer';
import {loadReducer} from "./loadReducer";

export const rootReducers = combineReducers({
  user: userReducer,
  driverTrucks: truckReducer,
  userLoads: loadReducer
});

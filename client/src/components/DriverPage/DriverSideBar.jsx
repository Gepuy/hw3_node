import React from 'react';
import {NavLink} from "react-router-dom";
import {LOADS_ROUTE, TRUCKS_ROUTE} from "../../utils/consts";
import {FaTruckLoading, FaTruckMoving} from "react-icons/fa";
import styles from './driverpage.module.css';

const DriverSideBar = () => {
  return (
    <div style={{width: "22%", borderRight: '2px solid #CFD2CF'}}>
      <div className={'d-flex flex-column'}
           style={{fontSize: "19px"}}
      >
        <NavLink
          to={TRUCKS_ROUTE}
          className={(navData) => navData.isActive ? styles.active : styles.navlink}
        >
          <FaTruckMoving/> My trucks
        </NavLink>
        <NavLink
          to={LOADS_ROUTE}
          className={(navData) => navData.isActive ? styles.active : styles.navlink}
        >
          <FaTruckLoading/> Loads info
        </NavLink>
      </div>
    </div>
  );
};

export default DriverSideBar;

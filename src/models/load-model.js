/* eslint-disable indent */
const { Schema, model } = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const loadJoiSchema = Joi.object({
    created_by: Joi.objectId(),
    assigned_to: Joi.objectId(),
    name: Joi.string().required(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().min(10).required(),
    delivery_address: Joi.string().min(10).required(),
    dimensions: {
        width: Joi.number().required(),
        length: Joi.number().required(),
        height: Joi.number().required(),
    },
});

const LoadSchema = new Schema({
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
    },
    assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        default: null,
    },
    status: {
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        default: 'NEW',
    },
    state: {
        type: String,
        enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    pickup_address: {
      type: String,
      required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        width: { type: Number, required: true },
        length: { type: Number, required: true },
        height: { type: Number, required: true },
    },
    logs: {
        message: { type: String },
        time: { type: Date },
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
}, { versionKey: false });

module.exports = {
    Load: model('Load', LoadSchema),
    loadJoiSchema,
};

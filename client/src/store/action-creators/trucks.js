import {ActionTypes} from "../consts/ActionTypes";
import {addUserTruck, assignTruckToUser, deleteUserTruck, getUserTrucks} from "../../api/truckApi";

export const setDriverTrucks = () => {
  return async (dispatch) => {
    let response = await getUserTrucks();
    dispatch({type: ActionTypes.SET_DRIVER_TRUCKS, payload: response.trucks});
  };
};

export const addNewUserTruck = (type) => {
  return async (dispatch) => {
    await addUserTruck(type);
    dispatch(setDriverTrucks());
  };
};

export const removeUserTruck = (id) => {
  return async (dispatch) => {
    await deleteUserTruck(id);
    dispatch({type: ActionTypes.DELETE_USER_TRUCK, payload: id});
  };
};

export const assignTruck = (id) => {
  return async dispatch => {
    await assignTruckToUser(id);
    dispatch(setDriverTrucks());
  }
};

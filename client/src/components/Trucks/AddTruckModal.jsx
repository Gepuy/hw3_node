import React, {useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {useAction} from "../../store/hooks/useAction";

const AddTruck = ({show, onHide}) => {
  let [type, setType] = useState('');
  const {addNewUserTruck} = useAction();

  const addTruck = (type) => {
    addNewUserTruck(type).then(() => {
      setType('');
      onHide();
    })
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size={'sm'}
      centered
    >
      <Modal.Header closeButton style={{textAlign: "center"}}>
        <Modal.Title>Create new truck</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form className={'d-flex justify-content-center'}>
          <Dropdown className={'mt-2'}>
            <Dropdown.Toggle>{type || "Choose type"}</Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => setType('SPRINTER')}>Sprinter</Dropdown.Item>
              <Dropdown.Item onClick={() => setType('SMALL STRAIGHT')}>Small straight</Dropdown.Item>
              <Dropdown.Item onClick={() => setType('LARGE STRAIGHT')}>Large straight</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={'outline-secondary'} onClick={onHide}>Close</Button>
        <Button variant={'outline-success'} onClick={() => addTruck(type)}>Add Truck</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddTruck;

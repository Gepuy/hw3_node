/* eslint-disable class-methods-use-this,no-return-await */
const nodemailer = require('nodemailer');

class Mail {
  async sendNewPassword(email, password) {
    const emailTransporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL_LOGIN,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    return await emailTransporter.sendMail({
      from: process.env.EMAIL_LOGIN,
      to: email,
      subject: 'YOUR NEW PASSWORD',
      text: 'New password',
      html: `<b>Hello! Your new password is: <br> ${password}</br>`,
    });
  }
}

module.exports = new Mail();

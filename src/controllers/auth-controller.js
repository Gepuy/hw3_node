/* eslint-disable */
const { userJoiSchema} = require('../models/user-model');
const ApiError = require("../error/ApiError");
const User = require('../dao/user');
const Mail = require('../dao/mail');
const {UserWithEmailAlreadyExists} = require('../error/daoError');
const bcrypt = require("bcryptjs");
const getNewGeneratedPassword = require('../utils/generatePassword');
const generateAccessToken = require('../utils/generateAccessToken');


class AuthController {
  async registration(req, res, next) {
      const {email, password, role} = req.body;
      try {
        await userJoiSchema.validateAsync({email, password, role});
        const hashPassword = bcrypt.hashSync(password, 7);
        await User.create(email, hashPassword, role);
      } catch (e) {
        if(e === UserWithEmailAlreadyExists) {
          return next(ApiError.badRequest('User with this email already exists'));
        }
        return next(ApiError.badRequest(e.message))
      }
      return res.json({message: "Profile created successfully"});
  };

  async login(req, res, next) {
      const {email, password} = req.body;
      try {
        await userJoiSchema.extract(['email']).validateAsync(email);
        await userJoiSchema.extract(['password']).validateAsync(password);
          const user = await User.get(email);
          const isPassEquals = await bcrypt.compare(password, user.password);
          if (!isPassEquals) {
              return next(ApiError.badRequest('Incorrect password'));
          }
          const token = generateAccessToken(user._id, user.role, user.email, user.createdDate);
          return res.json({ jwt_token: token });
      } catch (e) {
        return next(ApiError.badRequest(e.message))
      }
  }

  async forgotPassword(req, res, next) {
    try {
      const { email } = req.body;
      await userJoiSchema.extract(['email']).validateAsync(email);

      const user = await User.get(email);

      const newGeneratedPassword = getNewGeneratedPassword();
      const newHashedPassword = bcrypt.hashSync(newGeneratedPassword, 7);
      await User.update(user.id, {password: newHashedPassword});
      await Mail.sendNewPassword(email, newGeneratedPassword);

      res.json({message: 'New password sent to your email address' });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new AuthController();

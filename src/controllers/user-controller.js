/* eslint-disable require-jsdoc,class-methods-use-this,consistent-return */
const bcrypt = require('bcryptjs');
const uuid = require('uuid');
const path = require('path');

const User = require('../dao/user');
const ApiError = require('../error/ApiError');

class UserController {
  // eslint-disable-next-line no-unused-vars
  async getProfileInfo(req, res, next) {
    const user = await User.getById(req.user.id);

    res.json({
      user: {
        _id: user.id,
        role: user.role,
        email: user.email,
        img: user.img,
        createdDate: user.createdDate,
      },
    });
  }

  // eslint-disable-next-line no-unused-vars
  async deleteProfile(req, res, next) {
    await User.delete(req.user.id);
    return res.json({ message: 'Profile deleted successfully' });
  }

  async changeUserPassword(req, res, next) {
    const { oldPassword, newPassword } = req.body;

    if (!oldPassword || !newPassword) {
      return next(ApiError.badRequest('Please enter data into both fields'));
    }

    const user = await User.getById(req.user.id);
    const isPasswordCorrect = await bcrypt.compare(oldPassword, user.password);

    if (!isPasswordCorrect) {
      return next(ApiError.badRequest('Current password is incorrect'));
    }

    const hashedPassword = await bcrypt.hash(newPassword, 7);
    await User.update(user.id, { password: hashedPassword });

    return res.json({ message: 'Password changed successfully' });
  }

  async updateUserPhoto(req, res, next) {
    try {
      const { img } = req.files;
      const filename = `${uuid.v4()}.jpg`;
      await img.mv(path.resolve(__dirname, '..', 'static', filename));
      await User.updatePhoto(req.user.id, filename);
      return res.json({ message: 'Photo successfully updated' });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new UserController();

import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {useAction} from "../../../store/hooks/useAction";

const UpdatePhoto = ({show, onHide}) => {
  const [file, setFile] = useState(null)
  const [error, setError] = useState('');
  const {updateUserPhoto} = useAction();


  const selectFile = e => {
    setFile(e.target.files[0]);
  }

  const updatePhoto = async (file) => {
    try {
      const formData = new FormData();
      formData.append('img', file);
      await updateUserPhoto(formData);
      setFile(null);
      onHide();
    } catch (e) {
      setError(e.response.data.message);
    }
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size={'sm'}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Forget password?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control
            className={'mt-3'}
            placeholder={'Enter user picture'}
            type={'file'}
            onChange={selectFile}
          />
        </Form>
        {error ? <p style={{margin: "0", color: "red"}}>{error}</p> : null}
      </Modal.Body>
      <Modal.Footer>
        <Button variant={'outline-secondary'} onClick={onHide}>Close</Button>
        <Button
          variant={'outline-success'}
          onClick={() => updatePhoto(file)}
        >Confirm</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UpdatePhoto;

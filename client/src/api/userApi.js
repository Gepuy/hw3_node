import {$authHost, $host} from "./index";
import jwt_decode from "jwt-decode";

export const registration = async (email, password, role) => {
  const {data} = await $host.post('api/auth/register', {email, password, role});
  return data;
};

export const login = async (email, password) => {
  const {data} = await $host.post('api/auth/login', {email, password});
  localStorage.setItem('token', data.jwt_token);
  return jwt_decode(data.jwt_token);
};

export const forgetPassword = async (email) => {
  await $authHost.post('api/auth/forgot_password', {email});
};

export const getUserInfo = async () => {
  const {data} = await $authHost.get('api/users/me');
  return data;
};

export const deleteUserProfile = async () => {
  await $authHost.delete('api/users/me');
};

export const changeUserPassword = async (oldPassword, newPassword, user) => {
  await $authHost.patch('api/users/me', {oldPassword, newPassword, user});
};

export const attachUserPhoto = async (files) => {
  await $authHost.put('/api/users/me/photo', files, {
    headers: {
      "Content-Type": "multipart/form-data",
    }
  } );
}

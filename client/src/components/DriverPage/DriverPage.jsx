import React from 'react';
import DriverSideBar from "./DriverSideBar";
import DriverRouter from "./DriverRouter";

const DriverPage = () => {
  return (
    <>
      <div className={'d-flex'} style={{minHeight: window.innerHeight - 54.5}}>
          <DriverSideBar />
        <div style={{width: "78%"}}>
          <DriverRouter/>
        </div>
      </div>
    </>
  );
};

export default DriverPage;

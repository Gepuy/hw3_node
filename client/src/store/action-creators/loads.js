import {ActionTypes} from "../consts/ActionTypes";
import {addUserLoad, deleteUserLoad, getLoads, iterateToNextState, postUserLoad} from "../../api/loadApi";

export const setUserLoads = () => {
  return async (dispatch) => {
    let response = await getLoads();
    dispatch({type: ActionTypes.SET_USER_LOADS, payload: response.loads});
  };
};

export const addLoad = (data) => {
  return async (dispatch) => {
    await addUserLoad(data);
    dispatch(setUserLoads());
  }
};

export const postLoad = (id) => {
  return async (dispatch) => {
    await postUserLoad(id);
    dispatch(setUserLoads());
  }
};

export const deleteLoad = (id) => {
  return async (dispatch) => {
    await deleteUserLoad(id);
    dispatch(setUserLoads());
  }
};

export const iterateLoadState = () => {
  return async (dispatch) => {
    await iterateToNextState();
    dispatch(setUserLoads());
  }
};

import React from 'react';
import {Button} from "react-bootstrap";
import {useAction} from "../../store/hooks/useAction";
import {FaShuttleVan, FaTruck, FaTruckMoving} from "react-icons/fa";

const Truck = ({id, type, assigned_to, status}) => {
  const {removeUserTruck, assignTruck} = useAction();

  return (
    <div className={"mb-3 bg-light d-flex align-self-center align-items-center justify-content-between bg-white"}
         style={{
           boxShadow: "0px 0px 6px rgba(69, 55, 61, 0.7)",
           width: "90%",
           padding: "1rem",
           borderRadius: "10px",
         }}>
      {type === 'SPRINTER' && <FaShuttleVan size={25}/>}
      {type === 'SMALL STRAIGHT' && <FaTruck size={25}/>}
      {type === 'LARGE STRAIGHT' && <FaTruckMoving size={25}/>}
      <p style={{margin: 0}}>{type}</p>
      <p style={{margin: 0}}>Status: {status}</p>
      {assigned_to ? 'Already assigned' : 'Can be assigned'}
      <div>
        {
          assigned_to ? null :
            <Button
              variant={"outline-secondary"}
              onClick={() => assignTruck(id)}
            >Assign</Button>
        }
        <Button
          className={'ms-3'}
          variant={'outline-danger'}
          onClick={() => removeUserTruck(id)}
        >Delete Truck</Button>
      </div>
    </div>
  );
};

export default Truck;

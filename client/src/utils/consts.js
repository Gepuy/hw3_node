export const REGISTRATION_ROUTE = '/register';
export const LOGIN_ROUTE = '/login';
export const USER_ROUTE = '/user';
export const MAIN_ROUTE = '/*';
export const TRUCKS_ROUTE = '/trucks';
export const LOADS_ROUTE = '/loads';
export const ASSIGNED_LOADS_ROUTE = '/assigned'

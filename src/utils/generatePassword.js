/* eslint-disable arrow-body-style */
const passwordGenerator = require('generate-password');

const getNewGeneratedPassword = () => {
  return passwordGenerator.generate({
    length: 16,
    numbers: true,
  });
};

module.exports = getNewGeneratedPassword;

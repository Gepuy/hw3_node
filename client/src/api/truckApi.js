import { $authHost } from "./index";

export const getUserTrucks = async () => {
  const {data} = await $authHost.get('/api/trucks');
  return data;
}

export const addUserTruck = async (type) => {
  const {data} = await $authHost.post('/api/trucks', {type});
  return data;
}

export const deleteUserTruck = async (id) => {
  const {data} = await $authHost.delete(`/api/trucks/${id}`);
  return data;
}

export const assignTruckToUser = async (id) => {
  const {data} = await $authHost.post(`/api/trucks/${id}/assign`);
  return data;
}

const ApiError = require('../error/ApiError');

// eslint-disable-next-line func-names
module.exports = function (userRole) {
  // eslint-disable-next-line consistent-return
  return (req, res, next) => {
    const { role } = req.user;
    if (role !== userRole) {
      return next(ApiError.badRequest('You have not access to do it'));
    }
    next();
  };
};

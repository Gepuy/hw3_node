import {Routes, Route} from 'react-router-dom';
import {driverRoutes} from '../../utils/routes';

const DriverRouter = () => {
  return (
    <Routes>
      {driverRoutes.map(({path, Component}) => (
        <Route key={path} path={path} element={<Component/>} exact/>
      ))}
    </Routes>
  );
};

export default DriverRouter;

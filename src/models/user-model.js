/* eslint-disable linebreak-style */
/* eslint-disable indent */
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
    email: Joi.string()
        .email({
            minDomainSegments: 2,
        })
        .required(),
    password: Joi.string().pattern(/^[a-zA-Z0-9]{3,30}$/).required(),
    role: Joi.string().pattern(/^SHIPPER|DRIVER$/).required(),
});

const UserSchema = new Schema({
    role: {
        type: String,
        default: 'SHIPPER',
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    img: {
        type: String,
        default: null,
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
}, { versionKey: false });

module.exports = {
    User: model('Users', UserSchema),
    userJoiSchema,
};

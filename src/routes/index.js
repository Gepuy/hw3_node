const Router = require('express');
const AuthRouter = require('./authRouter');
const authMiddleware = require('../middleware/authMiddleware');
const UserRouter = require('./userRouter');
const TruckRouter = require('./truckRouter');
const LoadRouter = require('./loadRouter');
const roleMiddleware = require('../middleware/roleMiddleware');

const router = new Router();
const DRIVER = 'DRIVER';

router.use('/auth', AuthRouter);
router.use('/users', authMiddleware, UserRouter);
router.use('/trucks', authMiddleware, roleMiddleware(DRIVER), TruckRouter);
router.use('/loads', authMiddleware, LoadRouter);

module.exports = router;

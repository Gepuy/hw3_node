/* eslint-disable class-methods-use-this */
const { Load: LoadModel } = require('../../../models/load-model');
const {
  LoadNotFound, CannotDeleteLoad, CannotChangeLoad, TrackNotFound, LoadCannotBeDelivered,
} = require('../../../error/daoError');

class Load {
  async getAll(id, limit, offset, status) {
    let loads;
    if (status) {
      loads = await LoadModel.find({ created_by: id, status })
        .limit(limit)
        .skip(offset);
    } else {
      loads = await LoadModel.find({ created_by: id })
        .limit(limit)
        .skip(offset);
    }
    return loads;
  }

  async create(data) {
    await LoadModel.create(data);
  }

  async getUserAllAssigned(id, limit, offset, status) {
    let loads;
    if (status) {
      loads = await LoadModel.find({ assigned_to: id, status })
        .limit(limit)
        .skip(offset);
    } else {
      loads = await LoadModel.find({ assigned_to: id })
        .limit(limit)
        .skip(offset);
    }
    return loads;
  }

  async getOne(id) {
    const load = await LoadModel.findById(id);
    if (!load) {
      throw LoadNotFound;
    }
    return load;
  }

  async getActive(id) {
    const load = await LoadModel.findOne({ assigned_to: id, status: 'ASSIGNED' });
    if (!load) {
      throw LoadNotFound;
    }
    return load;
  }

  async updateById(id, data) {
    const load = await LoadModel.findById(id);
    if (!load) {
      throw LoadNotFound;
    }

    if (load.status !== 'NEW') {
      throw CannotChangeLoad;
    }
    await LoadModel.findByIdAndUpdate(id, data);
  }

  async updateByField(field, data) {
    const load = await LoadModel.updateOne(field, data);
    if (!load) {
      throw TrackNotFound;
    }
    return load;
  }

  async updateLoadStatus(loadID, status) {
    const load = await LoadModel.findByIdAndUpdate(loadID, {
      status,
      $push: { logs: { message: `Load status is: ${status}`, time: new Date() } },
    });

    if (!load) {
      throw TrackNotFound;
    }

    return load;
  }

  async updateDeliveredLoadState(userId, loadId, loadState) {
    const deliveredLoad = await LoadModel.findByIdAndUpdate(loadId, {
      status: 'SHIPPED',
      state: loadState,
      $push: { logs: { message: `Load status is: ${loadState}`, time: new Date() } },
    });
    return deliveredLoad;
  }

  async updateLoadStateLogs(userId, loadId, loadState) {
    const load = await LoadModel.findByIdAndUpdate(
      loadId,
      {
        state: loadState,
        $push: { logs: { message: `Load status is: ${loadState}`, time: new Date() } },
      },
    );
    if (!load) {
      throw LoadNotFound;
    }

    return load;
  }

  async deleteById(loadId) {
    const load = await LoadModel.findById(loadId);
    if (!load) {
      throw LoadNotFound;
    }
    if (load.status !== 'NEW' && load.status !== 'SHIPPED') {
      throw CannotDeleteLoad;
    }

    await LoadModel.findByIdAndDelete(loadId);
    return load;
  }

  async setAssigned(loadId, truck) {
    await LoadModel.findByIdAndUpdate(loadId, {
      $set: {
        status: 'ASSIGNED',
        assigned_to: truck.created_by,
        state: 'En route to Pick Up',
      },
      $push: {
        logs: {
          message: `Load assigned to driver with id ${truck.created_by}`,
          time: new Date(Date.now()),
        },
      },
    });
  }

  compareTruckAndLoadParams(truck, load) {
    if (
      load.payload > truck.capacity
      || load.dimensions.width > truck.width
      || load.dimensions.length > truck.length
      || load.dimensions.height > truck.height
    ) {
      throw LoadCannotBeDelivered;
    }
  }
}

module.exports = new Load();

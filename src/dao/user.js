/* eslint-disable */
const UserSource = require('./source/mongodb/User')

class User {
    constructor(source) {
        this.source = source;
    }

    async get(email) {
        return await this.source.get(email);
    }

    async create(email, hashPassword, role) {
        await this.source.create(email, hashPassword, role);
    }

    async getById(id) {
        return await this.source.getById(id);
    }

    async delete(id) {
        await this.source.delete(id);
    }

    async update(id, updateConfig) {
        await this.source.update(id, updateConfig);
    }

    async updatePhoto(id, imageName) {
        return this.source.updatePhoto(id, imageName);
    }
}

module.exports = new User(UserSource);

/* eslint-disable */
const {User: UserModel} = require('../../../models/user-model');
const {UserNotFound, UserWithEmailAlreadyExists} = require("../../../error/daoError");
const {MongoServerError} = require("mongodb");

class User {
  async get(email) {
    const user = await UserModel.findOne({ email });
    if (!user) {
      throw UserNotFound;
    }
    return user;
  }

  async getById(id) {
    const user = await UserModel.findById(id);
    if (!user) {
      throw UserNotFound;
    }
    return user;
  }

  async create(email, hashPassword, role) {
    try {
      await UserModel.create({ email, password: hashPassword, role });
    } catch (e) {
      if(e instanceof MongoServerError) {
        if(e.code === 11000) {
          throw UserWithEmailAlreadyExists;
        }
      }
    }
  }

  async delete(id) {
    await UserModel.findByIdAndDelete(id);
  }

  async update(id, updateConfig) {
    await UserModel.findByIdAndUpdate(id, updateConfig)
  }

  async updatePhoto(id, imageName) {
    await UserModel.findByIdAndUpdate(id, {img: imageName});
  }
}

module.exports = new User();

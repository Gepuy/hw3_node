import React, {useState} from 'react';
import {Button, Card, Form} from "react-bootstrap";
import {LOGIN_ROUTE, MAIN_ROUTE, REGISTRATION_ROUTE} from "../utils/consts";
import {NavLink, useLocation, useNavigate} from "react-router-dom";
import {login, registration} from "../api/userApi";
import {useAction} from "../store/hooks/useAction";
import UserRole from "../components/UserRole";
import ForgetPassword from "../components/Account/modals/ForgetPassword";


const AuthPage = () => {
  const {getAuthUserData, setUserAuthStatus} = useAction();
  const location = useLocation();
  const navigator = useNavigate();
  const isLogin = location.pathname === LOGIN_ROUTE;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [userRole, setUserRole] = useState('')
  const [error, setError] = useState('');
  const [forgetPasswordModal, setForgetPasswordModal] = useState(false);

  const auth = async () => {
    try {
      if (isLogin) {
        await login(email, password);
        await getAuthUserData();
        setUserAuthStatus(true);
        navigator(MAIN_ROUTE);
      } else {
        await registration(email, password, userRole);
        navigator(LOGIN_ROUTE);
      }
    } catch (e) {
      setError(e.response.data.message);
    }
  }

  return (
    <div
      className={'d-flex justify-content-center align-items-center bg-light'}
      style={{height: window.innerHeight - 54, width: "100vw"}}
    >
      <Card style={{width: 600}} className={"p-5"}>
        <h2 className={'text-center'}>{isLogin ? 'Login' : 'Registration'}</h2>
        <Form className={'d-flex flex-column'}>
          <Form.Control
            className={'mt-4'}
            placeholder={'Enter email...'}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onFocus={() => setError('')}
          />
          <Form.Control
            className={'mt-4'}
            placeholder={'Enter password...'}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type={"password"}
            onFocus={() => setError('')}
          />
          {!isLogin && <UserRole setUserRole={setUserRole}/>}
          {error ? <p style={{margin: "0", color: "red"}}>{error}</p> : null}
          <div className={'d-flex justify-content-between mt-4 pl-3 pr-3'}>
            {isLogin ?
              <div>
                Don't have an account? <NavLink to={REGISTRATION_ROUTE}>Register now!</NavLink>
              </div>
              :
              <div>
                Do you have an account? <NavLink to={LOGIN_ROUTE}>Sign in!</NavLink>
              </div>
            }
            <Button onClick={auth} variant={'outline-success'}>
              {isLogin ? 'Sign in' : 'Sign up'}
            </Button>
          </div>
          {isLogin &&
            <Button
              variant={"outline-secondary"}
              className={'mt-3 align-self-start'}
              onClick={() => setForgetPasswordModal(true)}
            >Forgot Password?</Button>}
        </Form>
      </Card>
      <ForgetPassword
        show={forgetPasswordModal}
        onHide={() => setForgetPasswordModal(false)}/>
    </div>
  );
};

export default AuthPage;

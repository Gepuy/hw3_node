import {$authHost} from "./index";

export const getLoads = async () => {
  const {data} = await $authHost.get('./api/loads');
  return data;
}

export const getActiveLoad = async () => {
  const {data} = await $authHost.get('/api/loads/active');
  return data;
}

export const iterateToNextState = async () => {
  const {data} = await $authHost.patch('/api/loads/active/state');
  return data;
}

export const addUserLoad = async (loadData) => {
  const {data} = await $authHost.post('/api/loads', loadData);
  return data;
}

export const updateUserLoad = async (id, loadData) => {
  const {data} = await $authHost.put(`/api/loads/${id}`, loadData);
  return data;
}

export const deleteUserLoad = async (id) => {
  const {data} = await $authHost.delete(`/api/loads/${id}`);
  return data;
}

export const postUserLoad = async (id) => {
  const {data} = await $authHost.post(`/api/loads/${id}/post`);
  return data;
}

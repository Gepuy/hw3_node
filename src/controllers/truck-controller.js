/* eslint-disable class-methods-use-this,no-unused-vars */
const ApiError = require('../error/ApiError');
const { truckJoiSchema } = require('../models/truck-model');
const Truck = require('../dao/truck');

class TruckController {
  async getUserTrucks(req, res, next) {
    const trucks = await Truck.getAll(req.user.id);
    res.json({ trucks });
  }

  async addTruckForUser(req, res, next) {
    const { type } = req.body;
    try {
      await truckJoiSchema.validateAsync({
        created_by: req.user.id,
        type,
        status: 'IS',
      });
      await Truck.create({
        created_by: req.user.id,
        type,
        status: 'IS',
      });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }

    return res.json({ message: 'Truck created successfully' });
  }

  async getOneUserTruck(req, res, next) {
    try {
      const truck = await Truck.getOne(req.params.id);
      return res.json({ truck });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async updateUserTruck(req, res, next) {
    try {
      await truckJoiSchema.extract(['type']).validateAsync(req.body.type);
      await Truck.updateById(req.params.id, req.body);
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Truck details changed successfully' });
  }

  async deleteUserTruck(req, res, next) {
    try {
      await Truck.delete(req.params.id);
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Truck deleted successfully' });
  }

  async assignTruckToUser(req, res, next) {
    try {
      const userId = req.user.id;
      const truckId = req.params.id;

      const currentAssignedTruck = await Truck.getAssignedTruck(userId);

      if (currentAssignedTruck) {
        await Truck.unassign(currentAssignedTruck.id);
      }

      await Truck.assign(userId, truckId);
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Truck assigned successfully' });
  }
}

module.exports = new TruckController();

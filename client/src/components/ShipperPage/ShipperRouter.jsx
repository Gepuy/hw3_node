import {Routes, Route} from 'react-router-dom';
import {shipperRoutes} from '../../utils/routes';

const ShipperRouter = () => {
  return (
    <Routes>
      {shipperRoutes.map(({path, Component}) => (
        <Route key={path} path={path} element={<Component/>} exact/>
      ))}
    </Routes>
  );
};

export default ShipperRouter;

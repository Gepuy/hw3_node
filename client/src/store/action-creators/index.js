import * as UserActionCreator from './user';
import * as TrucksActionCreator from './trucks'
import * as LoadsActionCreator from './loads'

const ActionCreators = {
  ...UserActionCreator,
  ...TrucksActionCreator,
  ...LoadsActionCreator
};

export default ActionCreators;

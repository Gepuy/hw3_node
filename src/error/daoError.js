const UserNotFound = new Error('User not found');
const UserWithEmailAlreadyExists = new Error('User with this email already exists');
const TruckNotFound = new Error('Truck was not found');
const TrucksNotFound = new Error('No available trucks found');
const LoadNotFound = new Error('Load was not found!1');
const ActiveLoadNotFound = new Error('Active load was not found');
const CannotChangeTruck = new Error('Can not change truck while its on load');
const CannotDeleteTruck = new Error('Can not delete truck while its on load');
const CannotChangeLoad = new Error('Can not change load while its active');
const CannotDeleteLoad = new Error('Can not delete load while its active');
const LoadCannotBeDelivered = new Error('Load for available trucks can not be delivered because of the load size');

module.exports = {
  UserNotFound,
  UserWithEmailAlreadyExists,
  TruckNotFound,
  TrucksNotFound,
  LoadNotFound,
  ActiveLoadNotFound,
  CannotChangeLoad,
  CannotDeleteLoad,
  CannotChangeTruck,
  CannotDeleteTruck,
  LoadCannotBeDelivered,
};

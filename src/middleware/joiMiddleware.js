const ApiError = require('../error/ApiError');

// eslint-disable-next-line consistent-return
module.exports = (schema, key) => (req, res, next) => {
  try {
    let requestBody = req.body;
    if (key) {
      requestBody = req.body[key];
    }
    if (!requestBody) {
      return next(ApiError.badRequest('No data'));
    }
    const { error } = schema.validate(requestBody);
    if (error) {
      return next(ApiError.badRequest(error.message));
    }
    next();
  } catch (err) {
    return next(err.message);
  }
};
